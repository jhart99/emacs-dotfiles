;;; package --- init-flyspell.el

;;; Commentary:
;;; Stuff for go major mode
;;; Code:
(use-package flyspell
  :ensure t
  :defer t
  :init
  (progn
    (add-hook 'prog-mode-hook 'flyspell-prog-mode)
    (add-hook 'text-mode-hook 'flyspell-mode)
    )
	:config
  (define-key flyspell-mouse-map [down-mouse-3] #'flyspell-correct-word)
    )
(provide 'init-flyspell)
;;; init-flyspell.el ends here
