;;; package --- init-speedbar.el

;;; Commentary:
;;; Stuff for org mode
;;; Code:
(use-package sr-speedbar
	)
(provide 'init-speedbar)
;;; init-speedbar.el ends here
