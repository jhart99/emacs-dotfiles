;;; package --- init-yas.el

;;; Commentary:
;;; Stuff for yasnippets
;;; Code:
(use-package yasnippet ; Snippets
  :ensure t
  :config (setq yas-verbosity 1
                yas-wrap-around-region t)
  (yas-reload-all)
  (yas-global-mode))
(use-package yasnippet-snippets
  :ensure t)
(provide 'init-yas)
;;; init-ess.el ends here
