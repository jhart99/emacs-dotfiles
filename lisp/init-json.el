;;; package --- init-json.el

;;; Commentary:
;;; Stuff for json major mode
;;; Code:
(use-package json-mode
	:mode "\\.json\\'"
	:config )

(provide 'init-json)
;;; init-json.el ends here
