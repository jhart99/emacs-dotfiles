;;; package --- init-fortune.el

;;; Commentary:
;;; Insert a fortune on startup
;;; Code:
;; Supply a random fortune cookie as the *scratch* message.
(defun my-fortune-scratch-message ()
  (interactive)
  (let ((fortune
         (when (executable-find "fortune")
           (with-temp-buffer
             (shell-command "fortune" t)
             (let ((comment-start ";;")
                   (comment-empty-lines t)
                   (tab-width 4))
               (untabify (point-min) (point-max))
               (comment-region (point-min) (point-max)))
             (delete-trailing-whitespace (point-min) (point-max))
             (concat (buffer-string) "\n")))))
    (if (called-interactively-p 'any)
        (insert fortune)
      fortune)))

;; initial-scratch-message
(let ((fortune (my-fortune-scratch-message)))
  (when fortune
    (setq initial-scratch-message fortune)))
(provide 'init-fortune)
;;; init-fortune.el ends here
