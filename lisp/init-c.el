;;; package --- setup up c environment

;;; Commentary:
;;; Load everything needed for c/c++ development

;;; Code:
(use-package company-c-headers
  :ensure t
	:init(add-to-list 'company-backends 'company-c-headers))

(add-hook 'c-mode-common-hook 'hs-minor-mode)

;; (setq c-default-style "linux")

(use-package cc-mode
  :bind ((:map c-mode-map ("<tab>" . company-complete))
         (:map c++-mode-map ("<tab>" . company-complete))))

(use-package clang-format
  :ensure t
  :config (setq clang-format-executable 'clang-format-10)
  :bind ((:map c-mode-map ("C-c C-f" . clang-format-region))
         (:map c++-mode-map ("C-c C-f" . clang-format-region))))
(use-package modern-cpp-font-lock
  :ensure t
	:init
	(modern-c++-font-lock-global-mode t))

(use-package cmake-ide
  :ensure t
  :after cc-mode
  :config
  (cmake-ide-setup))

;;(use-package lsp-mode
;;   :ensure t
;;  :hook ((c-mode . lsp)
;;         (cpp-mode .lsp)
;;         (lsp-mode . lsp-enable-which-key-integration))
;;  :commands lsp)

;;(use-package lsp-ui
;;  :ensure t
;;  :commands lsp-ui-mode)
;;(use-package dap-mode
;;  :ensure t)

(provide 'init-c)
;;; init-c.el ends here
