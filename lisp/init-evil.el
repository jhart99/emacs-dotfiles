;;; package --- Evil mode

;;; Commentary:
;;; Evil package loading and configuration

;;; Code:
(use-package undo-tree
	     :ensure t
	     :pin gnu)

(use-package evil
  :ensure t
  :init 
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))

(provide 'init-evil)
;;; init-evil.el ends here
