;;; package --  load helm

;;; Commentary:
;;; This loads helm in the fashion I like.

;;; Code:
(use-package helm
  :ensure t
  :bind (("M-x" . helm-M-x)
         ("C-x C-f" . helm-find-files)
         ("C-x b" . helm-buffers-list)
         ("C-x c o" . helm-occur)
         ("M-y" . helm-show-kill-ring)
         ("C-x r b" . helm-filteredbookmarks))
  :config (helm-mode 1))

(provide 'init-helm)
;;; init-helm.el ends here
