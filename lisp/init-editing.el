;;; package --- General editing  Config

;;; Commentary:
;;; General setting outside of packages

;;; Code:

(progn
  (dolist (mode '(tool-bar-mode scroll-bar-mode))
    (when (fboundp mode) (funcall mode -1))))

;; no splash screen drop us to an empty buffer
(setq inhibit-startup-screen t
			initial-buffer-choice nil)

;; simple y or n
(defalias 'yes-or-no-p 'y-or-n-p)

(setq-default
 indent-tabs-mode nil
 tab-width 2)

;; Make sure that everything is in utf-8 by default
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

;; automatically delete trailing whitespace from lines
(add-hook 'write-file-hooks 'delete-trailing-whitespace)

;; don't leave files without a final newline
(setq require-final-newline t)

(global-set-key [f1] 'shell)
(substitute-key-definition 'kill-buffer 'kill-buffer-and-window global-map)

;; always show line and columns
(setq line-number-mode t)
(setq column-number-mode t)

;; define a sensible default number of columns
(setq fill-column 80)

;; echo keystrokes so we can see what we are doing
(setq echo-keystrokes 0.1)

;; improve cursor scrolling
(setq auto-window-vscroll t)

;; highlight the matching paranthesis
(show-paren-mode 1)

;; set a default font and size that makes sense
(set-frame-font "DejaVu Sans Mono-12:antialias=none:hinting=true" nil t)


(provide 'init-editing)
;;; init-editing.el ends here
