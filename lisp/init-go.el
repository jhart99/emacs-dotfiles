;;; package --- init-go.el

;;; Commentary:
;;; Stuff for go major mode
;;; Code:
(use-package go-mode
  :mode "\\.go\\'"
	:config
	(add-hook 'before-save-hook 'gofmt-before-save))
(use-package company-go
  :mode "\\.go\\'"
  )
(provide 'init-go)
;;; init-go.el ends here
