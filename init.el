;;; package --- init.el

;;; Commentary:
;;; global init script for Emacs.

;;; Code:
;; general global variables and initialization

;; init packages
;; (package-initialize)
;; base load path
(add-to-list 'load-path "~/.emacs.d/lisp/")

;; start a server or use a server rather than reloading constantly
(when (require 'server nil t)
  (unless (server-running-p)
    (server-start)))

;; Deal with the backupfiles
(setq backup-directory-alist '(("." . "~/.emacs.d/saves")))
(setq backup-by-copying t)
(setq delete-old-versions t
			kept-new-versions 6
			kept-old-versions 2
			version-control t)
;; utf-8 mode please
(set-default-coding-systems 'utf-8)
;; begin initializing packages.  Use-package must be the first one since the others depend on it
(require 'init-use-package)
;; (require 'init-solarized)
(require 'init-zenburn)
(require 'init-editing)
(require 'init-flyspell)
(require 'init-evil)
(require 'init-folding)
(require 'init-helm)
(require 'init-sml)
(require 'init-docker)
(require 'init-kubernetes)
(require 'init-python)
(require 'init-flycheck)
(require 'init-yaml)
(require 'init-go)
(require 'init-c)
(require 'init-json)
(require 'init-js)
(require 'init-web)
(require 'init-ess)
(require 'init-markdown)
(require 'init-magit)
(require 'init-systemd)
(require 'init-terraform)
(xterm-mouse-mode)
(require 'init-cm)
(require 'init-fortune)
(require 'init-yas)
(require 'init-tex)

(provide 'init)
;;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t)
 '(package-selected-packages
   '(evil undo-tree systemd yapfify xpm smart-mode-line pytest py-yapf markdown-mode kubernetes importmagic helm hc-zenburn-theme flycheck evil-magit ess dockerfile-mode company-jedi company-go))
 '(safe-local-variable-values
   '((cmake-ide-project-dir . "/home/iamdelf/cisc187-fa20-fire10/project-extra-dna")
     (cmake-ide-build-dir . "/home/iamdelf/cisc187-fa20-fire10/project-extra-dna/build")
     (cmake-ide-project-dir . "/home/iamdelf/cisc187-fa20-fire10/lab13-list-struct")
     (cmake-ide-build-dir . "/home/iamdelf/cisc187-fa20-fire10/lab13-list-struct/build")
     (cmake-ide-project-dir . "/home/iamdelf/cisc187-fa20-fire10/lab12-tree-traversal")
     (cmake-ide-build-dir . "/home/iamdelf/cisc187-fa20-fire10/lab12-tree-traversal/build")
     (cmake-ide-project-dir . "/home/iamdelf/cisc187-fa20-fire10/project2-postfix-calc")
     (cmake-ide-build-dir . "/home/iamdelf/cisc187-fa20-fire10/project2-postfix-calc/build")
     (cmake-ide-project-dir . "/home/iamdelf/cisc187-fa20-fire10/lab11-tree")
     (cmake-ide-build-dir . "/home/iamdelf/cisc187-fa20-fire10/lab11-tree/build")
     (add-to-list 'auto-mode-alist
                  '("\\.h\\'" . c++-mode))
     (cmake-ide-project-dir . ".")
     (cmake-ide-build-dir . "build")
     (c++-mode)
     (eval if
           (string-match ".h$"
                         (buffer-file-name))
           (c++-mode)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
